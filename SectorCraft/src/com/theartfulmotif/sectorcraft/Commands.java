package com.theartfulmotif.sectorcraft;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands {

	Main mPlugin;
	
	public Commands(Main thePlugin)
	{
		mPlugin = thePlugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		Player player = null;
		if (sender instanceof Player) {
			player = (Player) sender;
		}
		
		// Only admins can do this
		if (player != null)
		{
			if (player.hasPermission("admin"))
			{
				if (cmd.getName().equalsIgnoreCase("hide"))
				{
					for (Player p : mPlugin.getServer().getOnlinePlayers())
					{
						p.hidePlayer(player);
					}
				}
				if (cmd.getName().equalsIgnoreCase("show"))
				{
					for (Player p : mPlugin.getServer().getOnlinePlayers())
					{
						p.showPlayer(player);
					}
				}
				if (cmd.getName().equalsIgnoreCase("creative"))
				{
					if (player.getGameMode() == GameMode.SURVIVAL)
						player.setGameMode(GameMode.CREATIVE);
					else
						player.setGameMode(GameMode.SURVIVAL);
				}
				if (cmd.getName().equalsIgnoreCase("day"))
				{
					player.getWorld().setFullTime(0);
				}
				if (cmd.getName().equalsIgnoreCase("night"))
				{
					player.getWorld().setFullTime(14000);
				}
				if (cmd.getName().equalsIgnoreCase("j"))
				{
					Location lookLoc = player.getTargetBlock(null, 500).getLocation();
					while(player.getWorld().getBlockAt(lookLoc).getTypeId() != 0 && player.getWorld().getBlockAt((int)lookLoc.getX(),(int)lookLoc.getY() + 1, (int)lookLoc.getZ()).getTypeId() != 0)
					{
						lookLoc.setY(lookLoc.getY() + 1);
					}
					lookLoc.setYaw(player.getLocation().getYaw());
					player.teleport(lookLoc);
				}
				if (cmd.getName().equalsIgnoreCase("sector-admin"))
				{
					if (args.length == 1)
					{
						if (args[0].equalsIgnoreCase("unclaim"))
						{
							if (mPlugin.mRegionManager.CheckLocation(player.getLocation()) == null)
							{
								player.sendMessage(ChatColor.RED + "This land is not claimed.");
								return false;
							} 
							mPlugin.mRegionManager.UnclaimRegion(player.getLocation());
							player.sendMessage(ChatColor.GREEN + "Chunk unclaimed.");
						}
						if (args[0].equalsIgnoreCase("done"))
						{
							mPlugin.mPlayerManager.GetPlayer(player).mClaimingSector = null;
							mPlugin.mPlayerManager.GetPlayer(player).mClaimingAsAdmin = false;
						}
					}
					if (args.length == 2)
					{
						if (args[0].equalsIgnoreCase("info"))
						{
							player.sendMessage(ChatColor.GREEN + "Owner: " + ChatColor.GOLD + mPlugin.mSectorManager.GetSector(args[1]).Owner);
							String members = "";
							Boolean first = true;
							for (String m : mPlugin.mSectorManager.GetSector(args[1]).Members)
							{
								if (!first)
									members += ", ";
								members += m;
								first = false;
							}
							player.sendMessage(ChatColor.GREEN + "Members: " + ChatColor.GOLD + members);
						}
						if (args[0].equalsIgnoreCase("create"))
						{
							if (mPlugin.mSectorManager.GetSector(args[1]) != null)
							{
								player.sendMessage(ChatColor.RED + "A sector with this name already exists.");
								return false;
							}
							mPlugin.mSectorManager.CreateSector(args[1]);
							player.sendMessage(ChatColor.GREEN + "Sector " + ChatColor.GOLD + args[1] + ChatColor.GREEN + " created!");
						}
						if (args[0].equalsIgnoreCase("claim"))
						{
							if (mPlugin.mSectorManager.GetSector(args[1]) == null)
							{
								player.sendMessage(ChatColor.RED + "Sector does not exist.");
								return false;
							}
							mPlugin.mRegionManager.ClaimRegion(player.getLocation(), args[1]);
							player.sendMessage(ChatColor.GREEN + "Chunk claimed for " + ChatColor.GOLD + mPlugin.mSectorManager.GetSector(args[1]).Name);
						}
						if (args[0].equalsIgnoreCase("edit"))
						{
							if (mPlugin.mSectorManager.GetSector(args[1]) == null)
							{
								player.sendMessage(ChatColor.RED + "Sector does not exist.");
								return false;
							}
							mPlugin.mPlayerManager.GetPlayer(player).mWorkingSector = mPlugin.mSectorManager.GetSector(args[1]);
							player.sendMessage(ChatColor.BLUE + "Now editting " + ChatColor.GOLD + mPlugin.mSectorManager.GetSector(args[1]).Name);
						}
						if (args[0].equalsIgnoreCase("disband"))
						{
							if (mPlugin.mSectorManager.GetSector(args[1]) != null)
							{
								String name = mPlugin.mSectorManager.GetSector(args[1]).Name;
								mPlugin.mSectorManager.RemoveSector(args[1]);
								mPlugin.getServer().broadcastMessage(ChatColor.DARK_PURPLE + name + " has been disbanded.");
							} else {
								player.sendMessage(ChatColor.RED + "This sector does not exist.");
							}
						}
						if (args[0].equalsIgnoreCase("massclaim"))
						{
							if (mPlugin.mSectorManager.GetSector(args[1]) != null)
							{
								mPlugin.mPlayerManager.GetPlayer(player).mClaimingSector = mPlugin.mSectorManager.GetSector(args[1]);
								mPlugin.mPlayerManager.GetPlayer(player).mClaimingAsAdmin = true;
								player.sendMessage(ChatColor.GREEN + "Mass-Claim started for " + ChatColor.GOLD + mPlugin.mSectorManager.GetSector(args[1]).Name);
								player.sendMessage(ChatColor.GREEN + "Walk to claim land.");
							}
						}
						if (args[0].equalsIgnoreCase("claimlarge"))
						{
							if (mPlugin.mSectorManager.GetSector(args[1]) != null)
							{
								mPlugin.mRegionManager.ClaimLargeRegion(player.getLocation(), args[1]);
								if (mPlugin.mRegionManager.ClaimLargeFirst != null)
								{
									player.sendMessage(ChatColor.BLUE + "Use this command again on the opposite corner.");
								} else {
									player.sendMessage(ChatColor.GREEN + "Chunks claimed for " + ChatColor.GOLD + mPlugin.mSectorManager.GetSector(args[1]).Name);
								}
							}
						}
						if (args[0].equalsIgnoreCase("togglepvp"))
						{
							Sector sector = mPlugin.mSectorManager.GetSector(args[1]);
							if (sector != null)
							{
								if (sector.PVP == null)
									sector.PVP = true;
								sector.PVP = !sector.PVP;
								if (sector.PVP)
									player.sendMessage(ChatColor.GREEN + "PVP turned " + ChatColor.RED + "on" + ChatColor.GREEN + " for " + ChatColor.GOLD + sector.Name);
								else
									player.sendMessage(ChatColor.GREEN + "PVP turned " + ChatColor.BLUE + "off" + ChatColor.GREEN + " for " + ChatColor.GOLD + sector.Name);
								mPlugin.mSectorManager.SaveAll();
							}
						}
					}
					if (args.length == 3)
					{
						if (args[0].equalsIgnoreCase("color"))
						{
							if (mPlugin.mSectorManager.GetSector(args[1]) != null)
							{
								int color = 0x000000;
								if (args[2].equalsIgnoreCase("white"))
								{
									color = 0xFFFFFF;
									mPlugin.mSectorManager.GetSector(args[1]).mChatColor = ChatColor.WHITE;
								}
								if (args[2].equalsIgnoreCase("blue"))
								{
									color = 0x0000FF;
									mPlugin.mSectorManager.GetSector(args[1]).mChatColor = ChatColor.BLUE;
								}
								if (args[2].equalsIgnoreCase("green"))
								{
									color = 0x00DD00;
									mPlugin.mSectorManager.GetSector(args[1]).mChatColor = ChatColor.GREEN;
								}
								if (args[2].equalsIgnoreCase("yellow") || args[2].equalsIgnoreCase("gold"))
								{
									color = 0xEEFF44;
									mPlugin.mSectorManager.GetSector(args[1]).mChatColor = ChatColor.GOLD;
								}
								if (args[2].equalsIgnoreCase("gray"))
								{
									color = 0x2F4F4F;
									mPlugin.mSectorManager.GetSector(args[1]).mChatColor = ChatColor.GRAY;
								}
								mPlugin.mSectorManager.SetSectorColor(args[1], color);
								player.sendMessage(ChatColor.GREEN + "Sector color changed for " + ChatColor.GOLD + mPlugin.mSectorManager.GetSector(args[1]).Name);
							} else {
								player.sendMessage(ChatColor.RED + "This sector does not exist.");
							}
						}
					}
				}
			}
			
			if (cmd.getName().equalsIgnoreCase("sector"))
			{
				if (args.length == 1)
				{
					if (args[0].equalsIgnoreCase("info"))
					{
						if (mPlugin.mSectorManager.GetPlayerSector(player.getName()) == null)
						{
							player.sendMessage(ChatColor.GREEN + "You are not currently a member of a sector.");
							player.sendMessage(ChatColor.DARK_PURPLE + "/sector help" + ChatColor.GREEN + " for a list of sector commands.");
						} else {
							player.sendMessage(ChatColor.GREEN + "Sector: " + ChatColor.GOLD + mPlugin.mSectorManager.GetPlayerSector(player.getName()).Name);
							player.sendMessage(ChatColor.GREEN + "Owner: " + ChatColor.GOLD + mPlugin.mSectorManager.GetPlayerSector(player.getName()).Owner);
							String members = "";
							Boolean first = true;
							for (String m : mPlugin.mSectorManager.GetPlayerSector(player.getName()).Members)
							{
								if (!first)
									members += ", ";
								members += m;
								first = false;
							}
							player.sendMessage(ChatColor.GREEN + "Members: " + ChatColor.GOLD + members);
							String bans = "";
							first = true;
							for (String b : mPlugin.mSectorManager.GetPlayerSector(player.getName()).Bans)
							{
								if (!first)
									bans += ", ";
								bans += b;
								first = false;
							}
							player.sendMessage(ChatColor.GREEN + "Banned: " + ChatColor.GOLD + bans);
						}
					}
					if (args[0].equalsIgnoreCase("help"))
					{
						player.sendMessage(ChatColor.GOLD + "-----------------Sector Help-----------------");
						player.sendMessage(ChatColor.GREEN + "Create a new sector: " + ChatColor.DARK_PURPLE + "/sector create <sector name>");
						player.sendMessage(ChatColor.GREEN + "Claim land: " + ChatColor.DARK_PURPLE + "/sector claim");
						player.sendMessage(ChatColor.GREEN + "Change sector color: " + ChatColor.DARK_PURPLE + "/sector color <color> (ex:blue,green,gold,etc.)");
					}
					if (args[0].equalsIgnoreCase("claim"))
					{
						if (mPlugin.mPlayerManager.CheckMoney(player, 50))
						{
							if (!mPlugin.mSectorManager.GetPlayerSector(player.getName()).Owner.equalsIgnoreCase(player.getName()))
							{
								player.sendMessage(ChatColor.RED + "You are not the owner of your sector.");
								return false;
							}
							if (mPlugin.mRegionManager.CheckLocation(player.getLocation()) != null)
							{
								player.sendMessage(ChatColor.RED + "This land has already been claimed.");
								return false;
							}
							mPlugin.mRegionManager.ClaimRegion(player.getLocation(), mPlugin.mSectorManager.GetPlayerSector(player.getName()).Name);
							player.sendMessage(ChatColor.GREEN + "Chunk claimed for " + ChatColor.GOLD + mPlugin.mSectorManager.GetPlayerSector(player.getName()).Name);
							mPlugin.mPlayerManager.RemoveMoney(player, 50);
						} else {
							player.sendMessage(ChatColor.RED + "You do not have 50 emeralds.");
						}
					}
					if (args[0].equalsIgnoreCase("expireinvites"))
					{
						if (mPlugin.mSectorManager.GetPlayerSector(player.getName()).Owner.equalsIgnoreCase(player.getName()))
						{
							mPlugin.mSectorManager.GetPlayerSector(player.getName()).Invites.clear();
							mPlugin.mSectorManager.SaveAll();
							player.sendMessage(ChatColor.GREEN + "All invites have been expired.");
						} else {
							player.sendMessage(ChatColor.RED + "You are not the owner of your sector.");
						}
					}
					if (args[0].equalsIgnoreCase("toggleopenjoin"))
					{
						Sector s = mPlugin.mSectorManager.GetPlayerSector(player.getName());
						if (s != null)
						{
							if (s.Owner.equalsIgnoreCase(player.getName()))
							{
								s.OpenJoin = !s.OpenJoin;
								if (s.OpenJoin)
									player.sendMessage(ChatColor.GREEN + "Open Join turned " + ChatColor.BLUE + "on" + ChatColor.GREEN + " for your sector.");
								else
									player.sendMessage(ChatColor.GREEN + "Open Join turned " + ChatColor.BLUE + "off" + ChatColor.GREEN + "for your sector.");
							} else {
								player.sendMessage(ChatColor.RED + "You are not the owner of your sector.");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in a sector.");
						}
					}
				}
				if (args.length == 2)
				{
					if (args[0].equalsIgnoreCase("create"))
					{
						if (mPlugin.mPlayerManager.CheckMoney(player, 100))
						{
							Sector s = mPlugin.mSectorManager.CreateSector(args[1]);
							s.AddMember(player, true);
							mPlugin.mSectorManager.SaveAll();
							mPlugin.getServer().broadcastMessage(ChatColor.GREEN + "The sector " + ChatColor.GOLD + args[1] + ChatColor.GREEN + " was just created by " + ChatColor.BLUE + player.getName() + "!");
							mPlugin.mPlayerManager.RemoveMoney(player, 100);
						} else {
							player.sendMessage(ChatColor.RED + "You do not have 100 emeralds.");
						}
					}
					if (args[0].equalsIgnoreCase("color"))
					{
						Sector s = mPlugin.mSectorManager.GetPlayerSector(player.getName());
						if (s != null)
						{
							if (s.Owner.equalsIgnoreCase(player.getName()))
							{
								int color = 0x000000;
								if (args[1].equalsIgnoreCase("white"))
								{
									color = 0xFFFFFF;
									mPlugin.mSectorManager.GetPlayerSector(player.getName()).mChatColor = ChatColor.WHITE;
								}
								if (args[1].equalsIgnoreCase("blue"))
								{
									color = 0x0000FF;
									mPlugin.mSectorManager.GetPlayerSector(player.getName()).mChatColor = ChatColor.BLUE;
								}
								if (args[1].equalsIgnoreCase("green"))
								{
									color = 0x00DD00;
									mPlugin.mSectorManager.GetPlayerSector(player.getName()).mChatColor = ChatColor.GREEN;
								}
								if (args[1].equalsIgnoreCase("yellow") || args[1].equalsIgnoreCase("gold"))
								{
									color = 0xEEFF44;
									mPlugin.mSectorManager.GetPlayerSector(player.getName()).mChatColor = ChatColor.GOLD;
								}
								if (args[1].equalsIgnoreCase("gray"))
								{
									color = 0x2F4F4F;
									mPlugin.mSectorManager.GetPlayerSector(player.getName()).mChatColor = ChatColor.GRAY;
								}
								mPlugin.mSectorManager.SetSectorColor(mPlugin.mSectorManager.GetPlayerSector(player.getName()).Name, color);
								player.sendMessage(ChatColor.GREEN + "Sector color changed for " + ChatColor.GOLD + mPlugin.mSectorManager.GetPlayerSector(player.getName()).Name);
							} else {
								player.sendMessage(ChatColor.RED + "You are not the owner of your sector.");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in a sector.");
						}
					}
					if (args[0].equalsIgnoreCase("invite"))
					{
						Sector s = mPlugin.mSectorManager.GetPlayerSector(player.getName());
						if (s != null)
						{
							if (s.Owner.equalsIgnoreCase(player.getName()))
							{
								if (mPlugin.getServer().getPlayer(args[1]) != null)
								{
									s.AddInvite(args[1]);
									player.sendMessage(ChatColor.GREEN + "You have invited " + ChatColor.BLUE + args[1] + ChatColor.GREEN + " to join your sector!");
									mPlugin.getServer().getPlayer(args[1]).sendMessage(ChatColor.GREEN + "You have been invited to join " + ChatColor.GOLD + s.Name + ChatColor.GREEN + "!");
									mPlugin.getServer().getPlayer(args[1]).sendMessage(ChatColor.DARK_PURPLE + "/sector join " + s.Name + ChatColor.GREEN + " to join!");
									mPlugin.mSectorManager.SaveAll();
								}
								else
								{
									player.sendMessage(ChatColor.RED + "This player does not exist.");
								}
							} else {
								player.sendMessage(ChatColor.RED + "You are not the owner of your sector.");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in a sector.");
						}
					}
					if (args[0].equalsIgnoreCase("kick"))
					{
						Sector s = mPlugin.mSectorManager.GetPlayerSector(player.getName());
						if (s != null)
						{
							if (s.Owner.equalsIgnoreCase(player.getName()))
							{
								if (s.Members.contains(args[1]))
								{
									s.Members.remove(args[1]);
									mPlugin.mSectorManager.SaveAll();
									mPlugin.getServer().getPlayer(args[1]).sendMessage(ChatColor.RED + "You have been kicked from " + ChatColor.GOLD + s.Name);
									player.sendMessage(ChatColor.GREEN + "You have kicked " + ChatColor.BLUE + args[1] + ChatColor.GREEN + " from your sector.");
								} else {
									player.sendMessage(ChatColor.RED + "This player is not a member of your sector.");
								}
							} else {
								player.sendMessage(ChatColor.RED + "You are not the owner of your sector.");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in a sector.");
						}
					}
					if (args[0].equalsIgnoreCase("join"))
					{
						Sector s = mPlugin.mSectorManager.GetSector(args[1]);
						if (s != null)
						{
							if (s.Bans.contains(player.getName()))
							{
								player.sendMessage(ChatColor.RED + "You have been banned from this sector.");
								return false;
							}
							if (s.OpenJoin)
							{
								s.AddMember(player, false);
								mPlugin.mSectorManager.SectorMessage(s.Name, ChatColor.BLUE + player.getName() + ChatColor.GREEN + " has joined " + ChatColor.GOLD + s.Name + ChatColor.GREEN + "!");
								mPlugin.mSectorManager.SaveAll();
							} else {
								if (s.Invites.contains(player.getName()))
								{
									s.AddMember(player, false);
									mPlugin.mSectorManager.SectorMessage(s.Name, ChatColor.BLUE + player.getName() + ChatColor.GREEN + " has joined " + ChatColor.GOLD + s.Name + ChatColor.GREEN + "!");
									mPlugin.mSectorManager.SaveAll();
								} else {
									player.sendMessage(ChatColor.RED + "This sector is invite only.");
								}
							}
						} else {
							player.sendMessage(ChatColor.RED + "This sector does not exist.");
						}
					}
					if (args[0].equalsIgnoreCase("quit"))
					{
						Sector s = mPlugin.mSectorManager.GetPlayerSector(player.getName());
						if (s != null)
						{
							if (!s.Owner.equalsIgnoreCase(player.getName()))
							{
								s.Members.remove(player.getName());
								player.sendMessage(ChatColor.GREEN + "You have quit " + ChatColor.GOLD + s.Name);
								mPlugin.mSectorManager.SectorMessage(s.Name, ChatColor.BLUE + player.getName() + ChatColor.RED + " has quit your sector.");
							} else {
								player.sendMessage(ChatColor.RED + "You cannot quit your own sector.");
								player.sendMessage(ChatColor.DARK_PURPLE + "/sector disband" + ChatColor.GREEN + " to disband your sector.");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in a sector.");
						}
					}
					if (args[0].equalsIgnoreCase("ban"))
					{
						Sector s = mPlugin.mSectorManager.GetPlayerSector(player.getName());
						if (s != null)
						{
							if (s.Owner.equalsIgnoreCase(player.getName()))
							{
								s.AddBan(args[1]);
								player.sendMessage(ChatColor.BLUE + args[1] + ChatColor.GREEN + " has been banned from your sector.");
								if (mPlugin.getServer().getPlayer(args[1]) != null)
									mPlugin.getServer().getPlayer(args[1]).sendMessage(ChatColor.RED + "You have been banned from " + ChatColor.GOLD + s.Name);
								mPlugin.mSectorManager.SaveAll();
								
							} else {
								player.sendMessage(ChatColor.RED + "You are not the owner of your sector.");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in a sector.");
						}
					}
					if (args[0].equalsIgnoreCase("unban"))
					{
						Sector s = mPlugin.mSectorManager.GetPlayerSector(player.getName());
						if (s != null)
						{
							if (s.Owner.equalsIgnoreCase(player.getName()))
							{
								if (s.Bans.contains(args[1]))
									s.Bans.remove(args[1]);
								player.sendMessage(ChatColor.BLUE + args[1] + ChatColor.GREEN + " has been unbanned from your sector.");
								if (mPlugin.getServer().getPlayer(args[1]) != null)
									mPlugin.getServer().getPlayer(args[1]).sendMessage(ChatColor.GREEN + "You have been unbanned from " + ChatColor.GOLD + s.Name);
								mPlugin.mSectorManager.SaveAll();
							} else {
								player.sendMessage(ChatColor.RED + "You are not the owner of your sector.");
							}
						} else {
							player.sendMessage(ChatColor.RED + "You are not in a sector.");
						}
					}
				}
			}
		}
		
		// For now permissions can only be added through the console
		if (player == null)
		{
			if (cmd.getName().equalsIgnoreCase("addpermission"))
			{
				if (args[1].equalsIgnoreCase("all"))
				{
					mPlugin.mPermissions.AddAllPermissions(args[0]);
					mPlugin.mPermissions.Save();
				} else {
					mPlugin.mPermissions.AddPermission(args[0], args[1]);
					mPlugin.mPermissions.Save();
				}
				return true;
			}
			
			if (cmd.getName().equalsIgnoreCase("removepermission"))
			{
				mPlugin.mPermissions.AddPermission(args[0], args[1], false);
				mPlugin.mPermissions.Save();
				return true;
			}
		} else {
			player.sendMessage("NO!");
		}
		
		return false;
	}
	
}
