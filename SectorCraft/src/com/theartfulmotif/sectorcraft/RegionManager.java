package com.theartfulmotif.sectorcraft;

import java.io.Serializable;
import java.util.ArrayList;

import org.bukkit.Chunk;
import org.bukkit.Location;

public class RegionManager {

	ArrayList<ChunkDefinition> mRegions;
	Main mPlugin;
	org.dynmap.markers.MarkerSet SectorsSet;
	int markerID = 0;
	
	@SuppressWarnings("unchecked")
	public RegionManager(Main thePlugin)
	{
		mPlugin = thePlugin;
		try {
			mRegions = (ArrayList<ChunkDefinition>) FileUtils.load("regions.dat");
			mPlugin.getLogger().info(String.valueOf(mRegions.size()) + " regions loaded.");
		} catch (Exception e) {
			mPlugin.getLogger().info("Error loading regions.");
			mRegions = new ArrayList<ChunkDefinition>();
		}
		SectorsSet = null;
		if (mPlugin.mDynmap.getMarkerAPI().getMarkerSet("sectors") != null)
		{
			mPlugin.mDynmap.getMarkerAPI().getMarkerSet("sectors").deleteMarkerSet();
		}
			
		SectorsSet = mPlugin.mDynmap.getMarkerAPI().createMarkerSet("sectors", "Sectors", null, true);

		for (ChunkDefinition c : mRegions)
		{
			AddMarker(c);
		}
	}
	
	public void AddMarker(ChunkDefinition c)
	{
		double[] x = new double[2];
		double[] z = new double[2];
		Chunk chunk = mPlugin.getServer().getWorld(c.world).getChunkAt(new Location(mPlugin.getServer().getWorld(c.world),c.locX, 0, c.locZ));
		x[0] = chunk.getX() * 16;
		x[1] = x[0] + 16;
		z[0] = chunk.getZ() * 16;
		z[1] = z[0] + 16;

		org.dynmap.markers.AreaMarker marker = SectorsSet.createAreaMarker(String.valueOf(markerID) + "region", c.sectorName, true, c.world, x, z, true);
		marker.setFillStyle(0.4, mPlugin.mSectorManager.GetSector(c.sectorName).Color);
		marker.setLineStyle(marker.getLineWeight(), marker.getLineOpacity(), mPlugin.mSectorManager.GetSector(c.sectorName).Color);
		c.dynmapRegionID = marker.getMarkerID();
		
		markerID ++;	
	}
	
	public void ClaimRegion(Location loc, String sectorName)
	{
		sectorName = mPlugin.mSectorManager.GetSector(sectorName).Name;
		ChunkDefinition c = new ChunkDefinition(loc, sectorName);
		AddMarker(c);
		mRegions.add(c);
		SaveAll();
	}
	
	public Location ClaimLargeFirst = null;
	public int ClaimLargeRegion(Location loc, String sectorName)
	{
		sectorName = mPlugin.mSectorManager.GetSector(sectorName).Name;
		if (ClaimLargeFirst == null)
		{
			ClaimLargeFirst = loc;
			return 0;
		} else {
			if (ClaimLargeFirst.getBlockX() < loc.getBlockX())
			{
				for (int x = ClaimLargeFirst.getBlockX(); x < loc.getBlockX(); x += 16)
				{
					if (ClaimLargeFirst.getBlockZ() < loc.getBlockZ())
					{
						for (int z = ClaimLargeFirst.getBlockZ(); z < loc.getBlockZ(); z += 16)
						{
							Location check = new Location(loc.getWorld(), x, 0, z);
							if (CheckLocation(check) == null)
							{
								ChunkDefinition c = new ChunkDefinition(check, sectorName);
								AddMarker(c);
								mRegions.add(c);
							}
						}
					} else {
						for (int z = loc.getBlockZ(); z < ClaimLargeFirst.getBlockZ(); z += 16)
						{
							Location check = new Location(loc.getWorld(), x, 0, z);
							if (CheckLocation(check) == null)
							{
								ChunkDefinition c = new ChunkDefinition(check, sectorName);
								AddMarker(c);
								mRegions.add(c);
							}
						}
					}
				}
			} else {
				for (int x = loc.getBlockX();x < ClaimLargeFirst.getBlockX(); x += 16)
				{
					if (ClaimLargeFirst.getBlockZ() < loc.getBlockZ())
					{
						for (int z = ClaimLargeFirst.getBlockZ(); z < loc.getBlockZ(); z += 16)
						{
							Location check = new Location(loc.getWorld(), x, 0, z);
							if (CheckLocation(check) == null)
							{
								ChunkDefinition c = new ChunkDefinition(check, sectorName);
								AddMarker(c);
								mRegions.add(c);
							}
						}
					} else {
						for (int z = loc.getBlockZ(); z < ClaimLargeFirst.getBlockZ(); z += 16)
						{
							Location check = new Location(loc.getWorld(), x, 0, z);
							if (CheckLocation(check) == null)
							{
								ChunkDefinition c = new ChunkDefinition(check, sectorName);
								AddMarker(c);
								mRegions.add(c);
							}
						}
					}
				}
			}
			ClaimLargeFirst = null;
			SaveAll();
			return 1;
		}
	}
	
	public void UnclaimRegion(Location loc)
	{
		ChunkDefinition cd = CheckLocationChunk(loc);
		SectorsSet.findAreaMarker(cd.dynmapRegionID).deleteMarker();
		mRegions.remove(cd);
		SaveAll();
	}
	
	public void SetSectorColor(String name, int color)
	{
		for (ChunkDefinition c : mRegions)
		{
			if (c.sectorName.equalsIgnoreCase(name))
			{
				org.dynmap.markers.AreaMarker m = SectorsSet.findAreaMarker(c.dynmapRegionID);
				m.setFillStyle(0.4, color);
				m.setLineStyle(m.getLineWeight(), m.getLineOpacity(), color);
			}
		}
	}
	
	public String CheckLocation(Location loc)
	{
		for (ChunkDefinition c : mRegions)
		{
			Location chunkLoc = new Location(mPlugin.getServer().getWorld(c.world), c.locX, 0, c.locZ);
			if (mPlugin.getServer().getWorld(c.world).getChunkAt(chunkLoc).equals(loc.getChunk()))
			{
				return c.sectorName;
			}
		}
		return null;
	}
	
	public ChunkDefinition CheckLocationChunk(Location loc)
	{
		for (ChunkDefinition c : mRegions)
		{
			Location chunkLoc = new Location(mPlugin.getServer().getWorld(c.world), c.locX, 0, c.locZ);
			if (mPlugin.getServer().getWorld(c.world).getChunkAt(chunkLoc).equals(loc.getChunk()))
			{
				return c;
			}
		}
		return null;
	}
	
	public void SaveAll()
	{
		try {
			FileUtils.save(mRegions, "regions.dat");
			mPlugin.getLogger().info("Regions saved.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static class ChunkDefinition implements Serializable
	{
		private static final long serialVersionUID = 7526471155622776147L;
		int locX;
		int locZ;
		String sectorName;
		String world;
		String dynmapRegionID;
		
		public ChunkDefinition(Location loc, String name)
		{
			locX = loc.getBlockX();
			locZ = loc.getBlockZ();
			sectorName = name;
			world = loc.getWorld().getName();
		}
	}
}
