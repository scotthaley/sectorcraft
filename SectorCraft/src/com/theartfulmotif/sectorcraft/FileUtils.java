package com.theartfulmotif.sectorcraft;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileUtils {
	
	// Creates the folder that will contain the plugin's files
	public static void createFolder() throws Exception
	{
        String path = new File(".").getAbsolutePath();
        File f = new File(path + "/plugins/SectorCraft");
        if(!f.exists())
        {
        	f.mkdir();
        }
	}

	public static void save(Object obj,String path) throws Exception
	{
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("plugins/SectorCraft/" + path));
		oos.writeObject(obj);
		oos.flush();
		oos.close();
	}
	
	public static Object load(String path) throws Exception
	{
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("plugins/SectorCraft/" + path));
		Object result = ois.readObject();
		ois.close();
		return result;
	}
}
