package com.theartfulmotif.sectorcraft;

import java.io.Serializable;
import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Sector  implements Serializable
{
	private static final long serialVersionUID = 7526471155622776147L;
	public String Name;
	public String Owner;
	public ArrayList<String> Members;
	public ArrayList<String> Invites;
	public ArrayList<String> Bans;
	public int Color = 0xFF0000;
	public ChatColor mChatColor = ChatColor.GOLD;
	public Boolean PVP = true;
	public Boolean OpenJoin = false;
	
	public Sector(String name)
	{
		Name = name;
		Members = new ArrayList<String>();
		Invites = new ArrayList<String>();
		Bans = new ArrayList<String>();
	}
	
	public void Fix()
	{
		// if I add new variables that need to be initialized
	}
	
	public void AddMember(Player player, Boolean owner)
	{
		Members.add(player.getName());
		if (owner)
			Owner = player.getName();
		if (Invites.contains(player.getName()))
			Invites.remove(player.getName());
	}
	
	public void AddInvite(String player)
	{
		if (!Invites.contains(player))
			Invites.add(player);
	}
	
	public void AddBan(String player)
	{
		if (Members.contains(player))
			Members.remove(player);
		if (Invites.contains(player))
			Invites.remove(player);
		if (!Bans.contains(player))
			Bans.add(player);
	}
}
