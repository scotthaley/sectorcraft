package com.theartfulmotif.sectorcraft;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Permissions {

	HashMap<String,HashMap<String,Boolean>> PermissionsMap;
	Main mPlugin;
	
	@SuppressWarnings("unchecked")
	public Permissions(Main thePlugin)
	{
		mPlugin = thePlugin;
		PermissionsMap = new HashMap<String,HashMap<String,Boolean>>();
		
		try {
			PermissionsMap = (HashMap<String,HashMap<String,Boolean>>)FileUtils.load("permissions.dat");
			for (Entry<String, HashMap<String, Boolean>> p : PermissionsMap.entrySet())
			{
				mPlugin.getLogger().info("Loading: " + p);
				for (Entry<String, Boolean> perm : p.getValue().entrySet())
				{
					Player player = Bukkit.getServer().getPlayer(p.getKey());
					if (player != null)
						player.addAttachment(mPlugin,perm.getKey(),perm.getValue());
				}
			}
			mPlugin.getLogger().info("SectorCraft permissions loaded!");
		} catch (Exception e) {
			mPlugin.getLogger().info("Error loading permissions: " + e.getMessage());
		}
	}
	
	public Boolean LoadPlayerPermissions(Player player)
	{
		if (PermissionsMap.containsKey(player.getName()))
		{
			for (Entry<String, Boolean> perm : PermissionsMap.get(player.getName()).entrySet())
			{
				if (player != null)
					player.addAttachment(mPlugin,perm.getKey(),perm.getValue());
			}
			player.addAttachment(mPlugin,"bukkit.command.kill",false);
			player.addAttachment(mPlugin,"bukkit.command.op",false);
			player.addAttachment(mPlugin,"bukkit.command.time",false);
			player.addAttachment(mPlugin,"bukkit.command.save",false);
			player.addAttachment(mPlugin,"bukkit.command.ban",false);
			player.addAttachment(mPlugin,"bukkit.command.unban",false);
			player.addAttachment(mPlugin,"bukkit.command.kick",false);
			player.addAttachment(mPlugin,"bukkit.command.*",false);
			return true;
		}
		return false;
	}
	
	public void AddAllPermissions(String player)
	{
		AddPermission(player,"bukkit.command.kill",true);
		AddPermission(player,"bukkit.command.op",true);
		AddPermission(player,"bukkit.command.time",true);
		AddPermission(player,"bukkit.command.save",true);
		AddPermission(player,"bukkit.command.ban",true);
		AddPermission(player,"bukkit.command.unban",true);
		AddPermission(player,"bukkit.command.kick",true);
		AddPermission(player,"bukkit.command.*",true);
	}
	
	public void AddPermission(String player, String permission)
	{
		AddPermission(player,permission,true);
	}
	
	public void AddPermission(String player, String permission, Boolean enabled)
	{
		if (!PermissionsMap.containsKey(player))
		{
			PermissionsMap.put(player, new HashMap<String,Boolean>());
			PermissionsMap.get(player).put("admin", false);
		}
		
		PermissionsMap.get(player).put(permission, enabled);
		
		Bukkit.getServer().getPlayer(player).addAttachment(mPlugin,permission,enabled);
		if (enabled)
			Bukkit.getLogger().info("Gave " + player + " the permission: " + permission);
		else
			Bukkit.getLogger().info("Removed " + permission + " from player: " + player);
	}
	
	public void SetDefault(String player)
	{
		if (!PermissionsMap.containsKey(player))
		{
			AddPermission(player,"admin",false);
		}
		Bukkit.getServer().getPlayer(player).addAttachment(mPlugin,"bukkit.command.kill",false);
	}
	
	public void Save()
	{
		try {
			FileUtils.save(PermissionsMap, "permissions.dat");
			Bukkit.getLogger().info("Permissions saved!");
		} catch (Exception e) {
			Bukkit.getLogger().info("Error: " + e.getMessage());
		}
	}
}
