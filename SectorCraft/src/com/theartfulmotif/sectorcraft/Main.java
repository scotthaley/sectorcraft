package com.theartfulmotif.sectorcraft;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin  {
	Commands mCommandHandler;
	public Permissions mPermissions;
	public SectorManager mSectorManager;
	public RegionManager mRegionManager;
	public PlayerManager mPlayerManager;
	public org.dynmap.DynmapAPI mDynmap;
	
	public void onEnable()
	{
		getLogger().info("SectorCraft has been enabled.");
		mDynmap = (org.dynmap.DynmapAPI) getServer().getPluginManager().getPlugin("dynmap");
		mCommandHandler = new Commands(this);
		mPermissions = new Permissions(this);
		mSectorManager = new SectorManager(this);
		mRegionManager = new RegionManager(this);
		mPlayerManager = new PlayerManager(this);
		
		try 
		{
			FileUtils.createFolder();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Bukkit.getServer().setSpawnRadius(0);
		
		this.getServer().getPluginManager().registerEvents(new WorldListener(this), this);
	}
	
	public void onDisable()
	{
		getLogger().info("SectorCraft has been disabled.");
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		mCommandHandler.onCommand(sender, cmd, commandLabel, args);
		return false;
	}
}
