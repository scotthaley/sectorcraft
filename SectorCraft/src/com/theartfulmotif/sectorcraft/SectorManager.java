package com.theartfulmotif.sectorcraft;

import java.util.ArrayList;

public class SectorManager {

	ArrayList<Sector> mSectors;
	Main mPlugin;
	
	@SuppressWarnings("unchecked")
	public SectorManager(Main thePlugin)
	{
		mPlugin = thePlugin;
		try {
			mSectors = (ArrayList<Sector>) FileUtils.load("sectors.dat");
			mPlugin.getLogger().info(String.valueOf(mSectors.size()) + " sectors loaded.");
		} catch (Exception e) {
			mPlugin.getLogger().info("Error loading sectors.");
			mSectors = new ArrayList<Sector>();
		}
		for (Sector s : mSectors)
			s.Fix();
	}
	
	public void RemoveSector(String name)
	{
		mSectors.remove(GetSector(name));
		SaveAll();
	}
	
	public Sector CreateSector(String name)
	{
		Sector newSector = new Sector(name);
		mSectors.add(newSector);
		SaveAll();
		return newSector;
	}
	
	public void SetSectorColor(String name, int color)
	{
		GetSector(name).Color = color;
		mPlugin.mRegionManager.SetSectorColor(name, color);
		SaveAll();
	}
	
	public void SectorMessage(String name, String message)
	{
		for (String p : GetSector(name).Members)
		{
			mPlugin.getServer().getPlayer(p).sendMessage(message);
		}
	}
	
	public Sector GetPlayerSector(String player)
	{
		for (Sector s : mSectors)
		{
			if (s.Members != null)
			{
				for (String p : s.Members)
				{
					if (p.equalsIgnoreCase(player))
						return s;
				}
			}
		}
		return null;
	}
	
	public Sector GetSector(String name)
	{
		if (name == null)
			return null;
		for (Sector s : mSectors)
		{
			if (name.equalsIgnoreCase(s.Name))
				return s;
		}
		return null;
	}
	
	public void SaveAll()
	{
		try {
			FileUtils.save(mSectors, "sectors.dat");
			mPlugin.getLogger().info("Sectors saved.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
