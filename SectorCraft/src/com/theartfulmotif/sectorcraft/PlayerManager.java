package com.theartfulmotif.sectorcraft;


import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PlayerManager {

	HashMap<Player, SectorCraftPlayer> mPlayers;
	Main mPlugin;
	
	public PlayerManager(Main thePlugin)
	{
		mPlugin = thePlugin;
		mPlayers = new HashMap<Player, SectorCraftPlayer>();
		for (Player p : mPlugin.getServer().getOnlinePlayers())
		{
			AddPlayer(p);
		}
	}
	
	public void AddPlayer(Player player)
	{
		mPlayers.put(player, new SectorCraftPlayer());
	}
	
	public void RemovePlayer(Player player)
	{
		mPlayers.remove(player);
	}
	
	public void UpdatePlayer(Player player)
	{
		if (!mPlayers.containsKey(player))
		{
			player.kickPlayer("Server reloaded.");
			return;
		}
		if (mPlayers.get(player).mSector != null)
		{
			Sector oldSector = mPlayers.get(player).mSector;
			mPlayers.get(player).mSector = mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(player.getLocation()));
			if (oldSector != mPlayers.get(player).mSector)
			{
				if (mPlayers.get(player).mSector != null)
					player.sendMessage(ChatColor.BLUE + "You have entered " + mPlayers.get(player).mSector.mChatColor + mPlayers.get(player).mSector.Name);
				else
					player.sendMessage(ChatColor.BLUE + "You have entered " + ChatColor.DARK_PURPLE + "Wilderness");
			} 
		} else {
			mPlayers.get(player).mSector = mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(player.getLocation()));
			if (mPlayers.get(player).mSector != null)
				player.sendMessage(ChatColor.BLUE + "You have entered " + mPlayers.get(player).mSector.mChatColor + mPlayers.get(player).mSector.Name);
		}
	}
	
	public boolean CheckMoney(Player player, int amount)
	{
		int total = 0;
		for (ItemStack item : player.getInventory().getContents())
		{
			if (item != null && item.getType().equals(Material.EMERALD))
			{
				total += item.getAmount();
			}
		}
		if (total >= amount)
			return true;
		else
			return false;
	}
	
	public void RemoveMoney(Player player, int amount)
	{
		int removed = 0;
		ItemStack item[] = player.getInventory().getContents();
		for (int i = 0; i < item.length; i ++)
		{
			if (item[i] != null && item[i].getType().equals(Material.EMERALD))
			{
				if (item[i].getAmount() + removed > amount)
				{
					item[i].setAmount(item[i].getAmount() + removed - amount);
				}
				else
				{
					removed += item[i].getAmount();
					player.getInventory().remove(item[i]);
				}
			}
		}
	}
	
	public SectorCraftPlayer GetPlayer(Player player)
	{
		if (mPlayers.containsKey(player))
			return mPlayers.get(player);
		return null;
	}
	
	public class SectorCraftPlayer
	{
		Sector mSector;
		public Sector mWorkingSector;
		public Sector mClaimingSector;
		public Boolean mClaimingAsAdmin = false;
	}
	
}
