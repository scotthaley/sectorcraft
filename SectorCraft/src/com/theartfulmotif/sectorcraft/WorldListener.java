package com.theartfulmotif.sectorcraft;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

public class WorldListener implements Listener {

	Main mPlugin;
	
	public WorldListener(Main thePlugin)
	{
		mPlugin = thePlugin;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		if (!mPlugin.mPermissions.LoadPlayerPermissions(event.getPlayer()))
			mPlugin.mPermissions.SetDefault(event.getPlayer().getName());
		mPlugin.mPlayerManager.AddPlayer(event.getPlayer());
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		mPlugin.mPlayerManager.RemovePlayer(event.getPlayer());
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event)
	{
		if (mPlugin.mPlayerManager.GetPlayer(event.getPlayer()).mClaimingSector != null)
		{
			if (mPlugin.mPlayerManager.GetPlayer(event.getPlayer()).mClaimingAsAdmin)
			{
				if (mPlugin.mRegionManager.CheckLocation(event.getPlayer().getLocation()) == null)
				{
					mPlugin.mRegionManager.ClaimRegion(event.getPlayer().getLocation(), mPlugin.mPlayerManager.GetPlayer(event.getPlayer()).mClaimingSector.Name);
					event.getPlayer().sendMessage(ChatColor.GREEN + "Chunk claimed for " + ChatColor.GOLD + mPlugin.mPlayerManager.GetPlayer(event.getPlayer()).mClaimingSector.Name);
				}
			}
		}
		mPlugin.mPlayerManager.UpdatePlayer(event.getPlayer());
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event)
	{
		Sector playerSector = mPlugin.mSectorManager.GetPlayerSector(event.getPlayer().getName());
		if (mPlugin.mRegionManager.CheckLocation(event.getBlock().getLocation()) != null && 
				mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(event.getBlock().getLocation())) != mPlugin.mPlayerManager.GetPlayer(event.getPlayer()).mWorkingSector &&
				playerSector != mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(event.getBlock().getLocation())))
		{
			event.getPlayer().sendMessage(ChatColor.RED + "You are not a member of this sector and cannot place blocks on its land.");
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event)
	{
		Sector playerSector = mPlugin.mSectorManager.GetPlayerSector(event.getPlayer().getName());
		if (mPlugin.mRegionManager.CheckLocation(event.getBlock().getLocation()) != null && 
				mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(event.getBlock().getLocation())) != mPlugin.mPlayerManager.GetPlayer(event.getPlayer()).mWorkingSector &&
						playerSector != mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(event.getBlock().getLocation())))
		{
			event.getPlayer().sendMessage(ChatColor.RED + "You are not a member of this sector and cannot destroy blocks on its land.");
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockBurn(BlockBurnEvent event)
	{
	}
	
	@EventHandler
	public void onBlockDamage(BlockDamageEvent event)
	{
		Sector playerSector = mPlugin.mSectorManager.GetPlayerSector(event.getPlayer().getName());
		if (mPlugin.mRegionManager.CheckLocation(event.getBlock().getLocation()) != null && 
				mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(event.getBlock().getLocation())) != mPlugin.mPlayerManager.GetPlayer(event.getPlayer()).mWorkingSector &&
						playerSector != mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(event.getBlock().getLocation())))
		{
			event.getPlayer().sendMessage(ChatColor.RED + "You are not a member of this sector and cannot destroy blocks on its land.");
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event)
	{
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK)
		{
			if (event.getClickedBlock().getType().equals(Material.WOODEN_DOOR))
			{
				Sector playerSector = mPlugin.mSectorManager.GetPlayerSector(event.getPlayer().getName());
				if (mPlugin.mRegionManager.CheckLocation(event.getClickedBlock().getLocation()) != null && 
						mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(event.getClickedBlock().getLocation())) != mPlugin.mPlayerManager.GetPlayer(event.getPlayer()).mWorkingSector &&
								playerSector != mPlugin.mSectorManager.GetSector(mPlugin.mRegionManager.CheckLocation(event.getClickedBlock().getLocation())))
				{
					event.getPlayer().sendMessage(ChatColor.RED + "You are not a member of this sector and cannot open doors on its land.");
					event.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event)
	{
		if (mPlugin.mRegionManager.CheckLocation(event.getLocation()) != null)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockRedstone(BlockRedstoneEvent event)
	{
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event)
	{
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event)
	{
	}

	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event)
	{
	}
	
	@EventHandler
	public void damage(EntityDamageByEntityEvent event)
	{
		if (event.getEntityType() == EntityType.PLAYER)
		{
			if (event.getDamager().getType() == EntityType.PLAYER)
			{
				if (!mPlugin.mPlayerManager.GetPlayer((Player)event.getEntity()).mSector.PVP || !mPlugin.mPlayerManager.GetPlayer((Player)event.getDamager()).mSector.PVP)
				{
					event.setCancelled(true);
				}
			} else {
				if (event.getDamager().getType() == EntityType.ARROW && ((Arrow)event.getDamager()).getShooter().getType() == EntityType.PLAYER)
				{
					if (!mPlugin.mPlayerManager.GetPlayer((Player)event.getEntity()).mSector.PVP || !mPlugin.mPlayerManager.GetPlayer((Player)((Arrow)event.getDamager()).getShooter()).mSector.PVP)
					{
						event.setCancelled(true);
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event)
	{
	}
	
	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent event)
	{
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event)
	{
	}
	
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event)
	{
	}
	
	@EventHandler
	public void onPlayerPickupItem(PlayerPickupItemEvent event)
	{
	}
	
	@EventHandler
	public void MOTD(ServerListPingEvent event)
	{
	}
	
	@EventHandler
	public void onSignChange(SignChangeEvent event)
	{
	}
	
	@EventHandler
	public void onVehicleDamage(VehicleDamageEvent event)
	{
	}
	
	@EventHandler
	public void onVehicleDestroy(VehicleDestroyEvent event)
	{

	}
	
	@EventHandler
	public void onVehicleExit(VehicleExitEvent event)
	{
	}
	
	@EventHandler
	public void onTeleport(PlayerTeleportEvent event)
	{
	  Player player = event.getPlayer();
	  for (Player p : Bukkit.getOnlinePlayers())
	  {
	    if (p.canSee(player))
	        p.showPlayer(player);
	  }
	}
}
